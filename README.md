# Alumni Network

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Java](https://img.shields.io/badge/-Java-red?logo=java)](https://www.java.com)
[![Spring](https://img.shields.io/badge/-Spring-white?logo=spring)](https://spring.io/)

 [ERD-model](https://drawsql.app/teams/team-404/diagrams/alumni) |
 [API docs](https://alumni-backend.herokuapp.com/api/v1/public/swagger-ui/index.html#/) | 
 [Front-end repository](https://gitlab.com/johyy/alumni-front) | 
 [Deployed front-end](https://alumni-fmt0.onrender.com/) | 

## Table of contents
* [Introduction](#introduction)
* [Features](#features)
* [Technologies](#technologies)
* [Usage](#usage)
* [Authors](#authors)
* [Sources](#sources)

## Introduction
 The goal of this project was to create an Alumni Network Portal and as per requirements 
 specification, we were to design and implement a software solution for facilitating 
 communication and event scheduling within groups (e.g., Java Fullstack '22), between groups 
 and with individual users. This repository considers the restful API of the product. The front-end
 repository can be found here: [alumni-front](https://gitlab.com/johyy/alumni-front) and
deployed client on Render [here](https://alumni-fmt0.onrender.com/).
 
 The API is a Spring Boot Gradle project written with Java. Spring security is used for authentication
and access-control in a stateless manner with JWTs. Dockerized Keycloak run on Heroku acts as 
 OAuth 2.0 authorization server & identity provider. API documentation can be found
 [here](https://alumni-backend.herokuapp.com/api/v1/public/swagger-ui/index.html#/).


## Features
- REST API
- Hibernate
- Protected endpoints, auth with JWT

## Technologies
- Java
- Spring
- Spring security
- OAuth 2.0
- Keycloak
- Heroku
- Docker
- PostgreSQL
- OpenAPI

## Usage
#### Requirements
- Postgres db
- OAuth 2.0 resource server

#### Configurations
- Configure application.properties so it matches your db and auth server.
- Do also necessary configurations to files in configs package.
- Hibernate is configured to create and db is seeded with mock data, change this behaviour as needed.

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run.


## Authors
[@Emil](https://gitlab.com/emilcalonius)<br />
[@Jani](https://gitlab.com/janijk)<br />
[@Jonna](https://gitlab.com/johyy)<br />
[@Laura](https://gitlab.com/Laura_Byman)<br />

## Sources
Project was the final project completed at the end of Java full stack
education program created by Noroff Education