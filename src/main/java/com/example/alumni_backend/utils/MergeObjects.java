package com.example.alumni_backend.utils;

import java.lang.reflect.Field;

public class MergeObjects {
    /**
     * Merges two objects of same type into one.
     * Update fields of param first with not null values of param second.
     * @param first object
     * @param second object
     * @return merged object
     * @param <T>
     */
    public static <T> T mergeObjects(T first, T second){
        Class<?> clas = first.getClass();
        Field[] fields = clas.getDeclaredFields();
        Object result = null;
        try {
            result = clas.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value1 = field.get(first);
                Object value2 = field.get(second);
                Object value = (value2 != null) ? value2 : value1;
                field.set(result, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) result;
    }
}

