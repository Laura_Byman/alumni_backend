package com.example.alumni_backend.utils;

public class ApiErrorResponse {
    private String timestamp;
    private Integer status;
    private String error;
    //private String trace; //Disable trace for production
    private String message;
    private String path;
}
