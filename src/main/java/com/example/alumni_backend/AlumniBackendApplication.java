package com.example.alumni_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlumniBackendApplication {
	public static void main(String[] args) {
		SpringApplication.run(AlumniBackendApplication.class, args);

	}
}
