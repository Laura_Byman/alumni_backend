package com.example.alumni_backend.controllers;

import com.example.alumni_backend.mappers.TopicMapper;
import com.example.alumni_backend.models.DTOs.EventDTO;
import com.example.alumni_backend.models.DTOs.NewTopicDTO;
import com.example.alumni_backend.models.DTOs.TopicDTO;
import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.Topic;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(origins = {
        /*"http://localhost:3000",
        "http://localhost:4200",*/
        "https://alumni-fmt0.onrender.com"})
@SecurityRequirement(name = "openApi")
@RequestMapping(path = "api/v1/topic")
public class TopicController {
    private final TopicMapper topicMapper;
    private final TopicService topicService;
    private final UserService userService;

    public TopicController(TopicMapper topicMapper, TopicService topicService, UserService userService) {
        this.topicMapper = topicMapper;
        this.topicService = topicService;
        this.userService = userService;
    }

    // Returns a list of topics
    // GET: api/v1/topic
    @Operation(summary = "Return all topics")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = TopicDTO.class)))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404", description = "No topics found", content = @Content)
    })
    @GetMapping()
    public ResponseEntity<Collection> getTopic(){
        return ResponseEntity.ok(topicMapper.topicToTopicDTO(topicService.findAll()));
    }

    // Returns a specific topic by its id
    // GET: api/v1/topic/{topic_id}
    @Operation(summary = "Return specific topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = TopicDTO.class))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "404", description = "Topics not found", content = @Content)
    })
    @GetMapping(path = "/{topic_id}")
    public ResponseEntity<TopicDTO> getSingleTopic(@PathVariable int topic_id){
        return ResponseEntity.ok(topicMapper.topicToTopicDTO(topicService.findById(topic_id)));
    }

    // Creates a new topic and also a subscribes the creator for it.
    // POST: api/v1/topic
    @Operation(summary = "Create a new topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "New topic created and creator subscribed to it",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = NewTopicDTO.class))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content)
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addTopic(
            @RequestBody NewTopicDTO newTopicDTO,
            @AuthenticationPrincipal Jwt jwt){
        if (newTopicDTO.getTitle() == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Title cannot be null");
        int posterId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        newTopicDTO.setTopicCreator(posterId);
        Topic topic = topicService.add(topicMapper.newTopicDTOToTopic(newTopicDTO));
        topicService.newUserTopicSubscription(topic.getId(),posterId);
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        URI location = URI.create(baseUrl+"/api/v1/topic/"+topic.getId());
        return ResponseEntity.created(location).body(topic.getId());
    }

    // Subscribe to a topic. topic_id from path, user from jwt.
    // POST: api/v1/topic/{topic_id}/join
    @Operation(summary = "Subscribe to a topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "New subscription created", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "404", description = "No topics found with provided ID", content = @Content)
    })
    @PostMapping(path = "/{topic_id}/join",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> subscribeToTopic(
            @PathVariable int topic_id,
            @AuthenticationPrincipal Jwt jwt){
        int posterId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        topicService.findById(topic_id);
        topicService.newUserTopicSubscription(topic_id,posterId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Delete user from a topic")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "User succesfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content)
    })
    @DeleteMapping({"/{topic_id}/leave", })
    public ResponseEntity leaveTopic(@PathVariable int topic_id, @AuthenticationPrincipal Jwt jwt) {

        int current_user_id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Topic topic = topicService.findById(topic_id);

        //creator of the topic cannot leave group
        if(current_user_id == topic.getTopicCreator().getId()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("The creator of the topic cannot leave created topic.");
        }

        topicService.deleteUserInTopic(current_user_id, topic_id);
        return ResponseEntity.noContent().build();
    }
}