package com.example.alumni_backend.controllers;

import com.example.alumni_backend.mappers.EventMapper;
import com.example.alumni_backend.models.DTOs.EventDTO;
import com.example.alumni_backend.models.DTOs.NewEventDTO;
import com.example.alumni_backend.models.Event;
import com.example.alumni_backend.services.Event.EventService;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.utils.MergeObjects;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin(origins = {
        /*"http://localhost:3000",
        "http://localhost:4200",*/
        "https://alumni-fmt0.onrender.com"})
@SecurityRequirement(name = "openApi")
@RequestMapping(path = "api/v1/event")
public class EventController {
    private final EventService eventService;
    private final UserService userService;
    private final GroupService groupService;
    private final TopicService topicService;
    private final EventMapper eventMapper;
    public EventController(EventService eventService, UserService userService, GroupService groupService,
                           TopicService topicService, EventMapper eventMapper) {
        this.eventService = eventService;
        this.userService = userService;
        this.groupService = groupService;
        this.topicService = topicService;
        this.eventMapper = eventMapper;
    }

    // Returns a list of events posted to groups and topics for which the requesting user is subscribed to.
    // GET: api/v1/event
    @Operation(summary = "Return all relevant events for requesting user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = EventDTO.class)))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404", description = "No events found", content = @Content)
    })
    @GetMapping()
    public ResponseEntity<?> getEvent(
            @AuthenticationPrincipal Jwt jwt){
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        return ResponseEntity.ok(eventMapper.eventToEventDTO(eventService.findGroupAndTopicEvents(userId)));
    }

    // Returns a specific event by its id.
    // GET: api/v1/event/{event_id}
    @Operation(summary = "Return event by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = EventDTO.class)))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404", description = "No events found", content = @Content)
    })
    @GetMapping(path = "/{event_id}")
    public ResponseEntity<?> getSpecificEvent(@PathVariable int event_id, @AuthenticationPrincipal Jwt jwt){
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        return ResponseEntity.ok(eventMapper.eventToEventDTO(eventService.findById(event_id)));
    }

    // Create new event. Params as application/json.
    // POST: api/v1/event
    @Operation(summary = "Create a new event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "New event created",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = EventDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
    })
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addEvent(
            @RequestBody NewEventDTO newEventDTO,
            @AuthenticationPrincipal Jwt jwt){
        if (newEventDTO.getTitle() == null)return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Title cannot be null");
        int posterId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Event event = eventMapper.newEventDTOToEvent(newEventDTO);
        event.setHost(posterId);
        Event newEvent = eventService.add(event);
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        URI location = URI.create(baseUrl+"/api/v1/event/"+newEvent.getId());
        return ResponseEntity.created(location).body(newEvent.getId());
    }

    // Update event, params as application/json. Only event creator may update, else 403.
    // PUT: api/v1/event/{event_id}
    @Operation(summary = "Update event info")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "Success",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = EventDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can do edits", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event not found", content = @Content),
    })
    @PutMapping(path = "/{id}")
    public ResponseEntity<?> updateEvent(
            @RequestBody NewEventDTO updates,
            @PathVariable int id,
            @AuthenticationPrincipal Jwt jwt){
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Event oldEvent = eventService.findById(id);
        if (callerId != oldEvent.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can do edits");
        }else {
            Event updatesToEvent = eventMapper.newEventDTOToEvent(updates);
            updatesToEvent.setHost(oldEvent.getId());
            updatesToEvent.setId(oldEvent.getId());
            Event updated = MergeObjects.mergeObjects(oldEvent, updatesToEvent);
            eventService.update(updated);
        }
        return ResponseEntity.noContent().build();
    }

    // New event group invitation. Only event creator may do invitations, else 403.
    // POST: api/v1/event/{event_id}/invite/group/{group_id}
    @Operation(summary = "Create new invitation to a group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Group invitation created",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can create invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or group not found", content = @Content),
    })
    @PostMapping(path = "/{event_id}/invite/group/{group_id}")
    public ResponseEntity<?> newGroupInvitation(
            @PathVariable int event_id,
            @PathVariable int group_id,
            @AuthenticationPrincipal Jwt jwt ) {
        Event event = eventService.findById(event_id);
        groupService.findById(group_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can create invitations");
        }else {
            eventService.createGroupInvite(group_id,event_id);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // Delete group invitation. Only event creator may delete, else 403. Doesn't delete RSVP records.
    // DELETE: api/v1/event/{event_id}/invite/group/{group_id}
    @Operation(summary = "Delete group invitation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "Success",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can delete invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or group not found", content = @Content),
    })
    @DeleteMapping(path = "/{event_id}/invite/group/{group_id}")
    public ResponseEntity<?> deleteGroupInvitation(
            @PathVariable int event_id,
            @PathVariable int group_id,
            @AuthenticationPrincipal Jwt jwt ){
        Event event = eventService.findById(event_id);
        groupService.findById(group_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can delete invitations");
        }else {
            eventService.deleteGroupInvite(group_id,event_id);
        }
        return ResponseEntity.noContent().build();
    }

    // New event topic invitation. Only event creator may do invitations, else 403.
    // POST: api/v1/event/{event_id}/invite/topic/{topic_id}
    @Operation(summary = "Create new invitation to a topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Topic invitation created",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can create invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or topic not found", content = @Content),
    })
    @PostMapping(path = "/{event_id}/invite/topic/{topic_id}")
    public ResponseEntity<?> newTopicInvitation(
            @PathVariable int event_id,
            @PathVariable int topic_id,
            @AuthenticationPrincipal Jwt jwt ) {
        Event event = eventService.findById(event_id);
        topicService.findById(topic_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can send invitations");
        }else {
            eventService.createTopicInvite(topic_id,event_id);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // Delete topic invitation. Only event creator may delete, else 403. Doesn't delete RSVP records.
    // DELETE: api/v1/event/{event_id}/invite/topic/{topic_id}
    @Operation(summary = "Delete topic invitation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "Success",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can delete invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or topic not found", content = @Content),
    })
    @DeleteMapping(path = "/{event_id}/invite/topic/{topic_id}")
    public ResponseEntity<?> deleteTopicInvitation(
            @PathVariable int event_id,
            @PathVariable int topic_id,
            @AuthenticationPrincipal Jwt jwt ){
        Event event = eventService.findById(event_id);
        topicService.findById(topic_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can delete invitations");
        }else {
            eventService.deleteTopicInvite(topic_id,event_id);
        }
        return ResponseEntity.noContent().build();
    }

    // New event user invitation. Only event creator may do invitations, else 403.
    // POST: api/v1/event/{event_id}/invite/user/{user_id}
    @Operation(summary = "Create new invitation to a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "User invitation created",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can create invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or user not found", content = @Content),
    })
    @PostMapping(path = "/{event_id}/invite/user/{user_id}")
    public ResponseEntity<?> newUserInvitation(
            @PathVariable int event_id,
            @PathVariable int user_id,
            @AuthenticationPrincipal Jwt jwt ) {
        Event event = eventService.findById(event_id);
        userService.findById(user_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can send invitations");
        }else eventService.createUserInvite(user_id,event_id);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // Delete user invitation. Only event creator may delete, else 403. Doesn't delete RSVP records.
    // DELETE: api/v1/event/{event_id}/invite/user/{user_id}
    @Operation(summary = "Delete user invitation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "Success",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Only event creator can delete invitations", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event or user not found", content = @Content),
    })
    @DeleteMapping(path = "/{event_id}/invite/user/{user_id}")
    public ResponseEntity<?> deleteUserInvitation(
            @PathVariable int event_id,
            @PathVariable int user_id,
            @AuthenticationPrincipal Jwt jwt ){
        Event event = eventService.findById(event_id);
        userService.findById(user_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (callerId != event.getHost()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only event creator can delete invitations");
        }else  eventService.deleteUserInvite(user_id,event_id);
        return ResponseEntity.noContent().build();
    }

    // New RSVP Record. user_id = requesting user. 403 if user not invited directly or via group/topic
    // POST: api/v1/event/{event_id}/rsvp
    @Operation(summary = "Create new RSVP")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "RSVP created",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403", description = "Can't respond to event without invitation", content = @Content),
            @ApiResponse(responseCode = "404", description = "Event not found", content = @Content),
    })
    @PostMapping(path = "/{event_id}/rsvp")
    public ResponseEntity<?> newRSVPRecord(
            @PathVariable int event_id,
            @AuthenticationPrincipal Jwt jwt ) {
        eventService.findById(event_id);
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        boolean hasInvitation = eventService.findIfUserHasInvitationForEvent(callerId,event_id);
        if(!hasInvitation) return ResponseEntity.status(HttpStatus.FORBIDDEN).body("No invitations found. You can't respond to this event");
        eventService.createRSVP(event_id,callerId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
