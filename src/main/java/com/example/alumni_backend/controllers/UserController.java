package com.example.alumni_backend.controllers;

import com.example.alumni_backend.mappers.UserMapper;
import com.example.alumni_backend.models.DTOs.*;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.utils.MergeObjects;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Set;

@RestController
@CrossOrigin(origins = {
        /*"http://localhost:3000",
        "http://localhost:4200",*/
        "https://alumni-fmt0.onrender.com"})
@SecurityRequirement(name = "openApi")
@RequestMapping(path = "api/v1/user")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    // No param get user. Gets user id from jwt and redirects to appropriate endpoint.
    // GET: api/v1/user
    @Operation(summary = "Return currently logged in user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content),
            @ApiResponse(responseCode = "303", description = "Redirect to user/{user_id}", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content)
    })
    @GetMapping()
    public ResponseEntity<String> getLoggedUser(@AuthenticationPrincipal Jwt jwt){
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        int id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        URI location = URI.create(baseUrl+"/api/v1/user/"+id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(location);
        return new ResponseEntity<>("", responseHeaders, HttpStatus.SEE_OTHER);
    }

    // Get user info. Returns restricted info if requester is not the owner of profile.
    // GET: api/v1/user/{user_id}
    @Operation(summary = "Return user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "403",description = "ID does not match with logged in user",content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable int id, @AuthenticationPrincipal Jwt jwt) {
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if(id != callerId) return ResponseEntity.ok(userMapper.userToPublicUserDTO(userService.findById(id)));
        else return ResponseEntity.ok(userMapper.userToUserDTO(userService.findById(id)));
    }

    // Get list of users
    // POST: api/v1/user/list
    @Operation(summary = "Return list of users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = PublicUserDTO.class)))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
    })
    @PostMapping(value = "/list",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserList(@RequestBody UserIdListDTO UserIds, @AuthenticationPrincipal Jwt jwt) {
        return ResponseEntity.ok(userMapper.userToPublicUserDTO(userService.findAllById(UserIds.getUserIds())));
    }

    // Update user info. Users can only edit their own info.
    // PATCH: api/v1/user/{user_id}
    @Operation(summary = "Update user info")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "User updated", content = @Content),
            @ApiResponse(responseCode = "403",description = "ID does not match with logged in user",content = @Content),
            @ApiResponse(responseCode = "400", description = "Client action error", content = @Content),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content)
    })
    @RequestMapping(value = "/{id}",method = RequestMethod.PATCH,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateUser(
             @RequestBody UpdateUserDTO body,
             @PathVariable int id,
             @AuthenticationPrincipal Jwt jwt) {
        int callerId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        if (!userService.exists(id))return ResponseEntity.badRequest().build();
        if(id != callerId) return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                "ID does not match with logged in user, it is not possible to edit info of other users");
        else {
            User updates = userMapper.updateUserDTOToUser(body);
            User oldUser = userService.findById(id);
            User updated = MergeObjects.mergeObjects(oldUser,updates);
            updated.setId(callerId);
            userService.update(updated);
        }
        return ResponseEntity.noContent().build();
    }
}
