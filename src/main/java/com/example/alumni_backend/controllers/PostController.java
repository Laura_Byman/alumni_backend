package com.example.alumni_backend.controllers;

import com.example.alumni_backend.mappers.PostMapper;
import com.example.alumni_backend.models.DTOs.NewPostDTO;
import com.example.alumni_backend.models.DTOs.PostDTO;
import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.Event.EventService;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.post.PostService;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin(origins = {
        /*"http://localhost:3000",
        "http://localhost:4200",*/
        "https://alumni-fmt0.onrender.com"})
@SecurityRequirement(name = "openApi")
@RequestMapping(path = "api/v1/post")
public class PostController {
    private final PostService postService;
    private final PostMapper postMapper;

    private final UserService userService;

    private final GroupService groupService;

    private final EventService eventService;
    private final TopicService topicService;

    public PostController(PostService postService, PostMapper postMapper, UserService userService,
                          GroupService groupService, EventService eventService, TopicService topicService) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.userService = userService;
        this.groupService = groupService;
        this.eventService = eventService;
        this.topicService = topicService;
    }

    @Operation(summary = "Get all posts by groups user has subscribed to.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
    public ResponseEntity findAllByUserSubscription(@AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Collection<Post> postsInGroups = postService.findAllByUserGroupSubscription(userId);
        Collection<Post> postsIntTopics = postService.findAllByUserTopicSubscription(userId);

        //combine topic and group posts to single collection
        Stream<Post> combinedStream = Stream.of(postsInGroups, postsIntTopics).flatMap(Collection::stream);
        List<Post> posts = combinedStream.collect(Collectors.toList());
        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return ResponseEntity.ok(postMapper.postToPostDto(posts));
    }

    @Operation(summary = "Get specific post.") // GET: api/v1/post/{post_id}
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Success",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",description = "Bad request",
                    content = { @Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class)) })})
    @GetMapping(path = "/{post_id}")
    public ResponseEntity findSpecificPost(@PathVariable int post_id, @AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Post post = postService.findById(post_id);

        Collection<Post> postsInGroups = postService.findAllByUserGroupSubscription(userId);
        Collection<Post> postsIntTopics = postService.findAllByUserTopicSubscription(userId);
        //combine topic and group posts to single collection
        Stream<Post> combinedStream = Stream.of(postsInGroups, postsIntTopics).flatMap(Collection::stream);
        List<Post> posts = combinedStream.toList();
        boolean isNotMember = posts.stream().filter(p -> p.getId() == post_id).findFirst().isEmpty();
        if (isNotMember == true)  return ResponseEntity.ok(postMapper.postToPostDto(post));
            else return ResponseEntity.notFound().build();
    }

    @Operation(summary = "add a post to group, topic or event")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Post successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody NewPostDTO newPostDTO, @AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();

        //if new post is to group
        if(newPostDTO.getTarget_group_id() !=0) {
            User userInGroup = groupService.findUserInGroup(userId,newPostDTO.getTarget_group_id());
            System.out.println("POST "+ userInGroup);
            if (userInGroup == null) {
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body("User is not allowed to post to selected group, the user is not a member of this group. ");
            }
            //to be implemented: find user subscribed to topic, check if user can post to selected topic
        }else if(newPostDTO.getTarget_topic_id() !=0){
            User userInTopic = topicService.findUserInTopic(userId,newPostDTO.getTarget_topic_id() );
            if(userInTopic == null){
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body("User is not allowed to post to selected topic, the user is not a subscribed to htis topic. ");
            }
        }else if(newPostDTO.getTarget_event_id() !=0){ // if new post is to event
            if(!eventService.findIfUserHasInvitationForEvent(userId, newPostDTO.getTarget_event_id())){
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body("User is not allowed to post to selected event, the user does not have invitation to selected event. ");
            }
        }
        newPostDTO.setAuthor(userId);
        Post post = postService.add(postMapper.newPostDtoToPost(newPostDTO));

        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        URI location = URI.create(baseUrl+"/api/v1/post/"+post.getId());
        return ResponseEntity.created(location).header("Access-Control-Allow-Headers",
                "Authorization, Access-Control-Allow-Headers, Origin, Accept, location, " +
                "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").body(post.getId());
    }

    @Operation(summary = "update a post")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Post successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Post not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
    })
    @PutMapping("/{post_id}")
    public ResponseEntity update(@RequestBody PostDTO postDTO, @PathVariable int post_id, @AuthenticationPrincipal Jwt jwt) {
        if(postDTO.getId() != post_id){
            return ResponseEntity.badRequest().build();
        }
        if (!postService.exists(post_id)) {
            return ResponseEntity.badRequest().build();
        }
        Post post = postService.findById(post_id);
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();

        if(post.getAuthor().getId() != userId) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The user is not the original author of this post, and is not allowed to edit it ");
        }
        if(post.getTarget_group() != null && post.getTarget_group().getId() != postDTO.getTarget_group_id()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The audience group of the post can not be changed after creation. ");
        }
        if(post.getTarget_topic() != null && post.getTarget_topic().getId() != postDTO.getTarget_topic_id()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The audience topic of the post can not be changed after creation. ");
        }
        if(post.getTarget_event() != null && post.getTarget_event().getId() != postDTO.getTarget_event_id()) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("The audience event of the post can not be changed after creation. ");
        }

        postService.update(postMapper.postDtoToPost(postDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all posts of selected event")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/event/{event_id}")
    public ResponseEntity findAllByEvent(@PathVariable int event_id) {
        Collection<Post> posts = postService.findAllByEvent(event_id);

        return ResponseEntity.ok(postMapper.postToPostDto(posts));
    }

    @Operation(summary = "Get all posts of selected topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/topic/{topic_id}")
    public ResponseEntity findAllByTopic(@PathVariable int topic_id) {
        Collection<Post> posts = postService.findAllByTopic(topic_id);

        return ResponseEntity.ok(postMapper.postToPostDto(posts));
    }

    @Operation(summary = "Get all posts of selected group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/group/{group_id}")
    public ResponseEntity findAllByGroup(@PathVariable int group_id) {
        Collection<Post> posts = postService.findAllByGroup(group_id);

        return ResponseEntity.ok(postMapper.postToPostDto(posts));
    }
}
