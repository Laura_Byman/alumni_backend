package com.example.alumni_backend.controllers;

import com.example.alumni_backend.mappers.GroupMapper;
import com.example.alumni_backend.models.DTOs.NewGroupDTO;
import com.example.alumni_backend.models.DTOs.GroupDTO;
import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {
        /*"http://localhost:3000",
        "http://localhost:4200",*/
        "https://alumni-fmt0.onrender.com"})
@SecurityRequirement(name = "openApi")
@RequestMapping(path = "api/v1/group")
public class GroupController {

    private final GroupService groupService;
    private final GroupMapper groupMapper;

    private final UserService userService;

    public GroupController(GroupService groupService, GroupMapper groupMapper, UserService userService) {
        this.groupService = groupService;
        this.groupMapper = groupMapper;
        this.userService = userService;
    }


    @Operation(summary = "Get all groups that are public and those private groups user is a member of")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Group.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
    public ResponseEntity findAllExcludePrivateUserNotMember( @AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Collection<Group> groups = groupService.findAllExcludePrivateGroupsThatUserIsNotMember(userId);

        return ResponseEntity.ok(groupMapper.groupToGroupDto(groups));
    }

    @Operation(summary = "Get a group by id, return forbidden if user if not a member of requested group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Group.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Group does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "403",
                    description = "The user is not a member of requested group",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id, @AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Group selectedGroup = groupService.findById(id);

        if(groupService.findUserInGroup(userId, selectedGroup.getId()) == null){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User is not a member of requested group");
        }

        GroupDTO group = groupMapper.groupToGroupDto(selectedGroup);
        return ResponseEntity.ok(group);
    }

    @Operation(summary = "add a group")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Group successfully added",
                    content = @Content),

            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@RequestBody NewGroupDTO newGroupDTO, @AuthenticationPrincipal Jwt jwt) {
        int userId = userService.findBySub(jwt.getClaimAsString("sub")).getId();

        //add user creating group  as a first member of created group
        Set<Integer> users = newGroupDTO.getUsers();
        users.add(userId);
        newGroupDTO.setUsers(users);
        //set creator
        newGroupDTO.setGroupCreator(userId);

        groupService.add(
                groupMapper.newGroupDtoToGroup(newGroupDTO)
        );

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "user join to a public group, or send a join request for private group")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "202",
                    description = "User has succesfully send a join request to private group.",
                    content = @Content),
            @ApiResponse(responseCode = "204",
                    description = "User succesfully joined",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "The group is private and a user cannot join private groups without invitation.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping({"/{group_id}/join", })
    public ResponseEntity joinGroup(@PathVariable int group_id, @AuthenticationPrincipal Jwt jwt) {

        int current_user_id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Group group = groupService.findById(group_id);

        if(group.is_private()){
            groupService.createGroupJoinRequest(current_user_id, group_id);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("The users join request succesfully accepted");
        }

        //user join to public group
        groupService.addUserToGroup(current_user_id, group_id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "add user to a group (if the user has an open join request)")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "User succesfully joined",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "The user to be added to group does not have an open join request or the current user" +
                    "is not allowed to add members to a selected private group",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping({ "/{group_id}/{user_id}/join"})
    public ResponseEntity addUserToGroup(@PathVariable int group_id, @PathVariable int user_id, @AuthenticationPrincipal Jwt jwt) {

        int current_user_id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Group group = groupService.findById(group_id);
        System.out.println("group" + group_id);

        //check if user has an open join request
        if(groupService.findUserGroupJoinRequest(user_id, group_id) == null){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("The user to be added to group does not have" +
                    "an open join request. An User cannot add other users to groups without open join request.");
        }

        //add new user to private group
        if (group.is_private()) {

            //user is part of private group and can add members, otherwise forbidden
            if(groupService.findUserInGroup(current_user_id, group_id) != null){
                groupService.addUserToGroup(user_id, group_id);
                groupService.deleteUserGroupJoinRequest(user_id, group_id);
                return ResponseEntity.noContent().build();
            }else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Current user is not a member of requested private group" +
                        "and cannot add members");
            }
        }

        //join user as member of the group and delete open join request
        groupService.addUserToGroup(user_id, group_id);
        groupService.deleteUserGroupJoinRequest(user_id, group_id);

        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "decline and delete users group join request.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Request deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "The current user is not a member of selected private group and cannot handle join requests",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping({ "/{group_id}/{user_id}/decline_request"})
    public ResponseEntity declineUserJoinRequest(@PathVariable int group_id, @PathVariable int user_id, @AuthenticationPrincipal Jwt jwt) {

        int current_user_id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Group group = groupService.findById(group_id);

        //add new user to private group
        if (group.is_private()) {

            //user is part of private group and can add members, otherwise forbidden
            if(groupService.findUserInGroup(current_user_id, group_id) != null){
                groupService.deleteUserGroupJoinRequest(user_id, group_id);
                return ResponseEntity.noContent().build();
            }else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Current user is not a member of requested private group" +
                        "and cannot handle join requests");
            }
        }

        //join user as member of the group and delete open join request
        groupService.deleteUserGroupJoinRequest(user_id, group_id);

        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete user from a group")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "User successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "401", description = "Insufficient authorization", content = @Content),
            @ApiResponse(responseCode = "403",
                    description = "The current user is a creator of the group. The creator cannot leave created group.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping({"/{group_id}/leave", })
    public ResponseEntity leaveGroup(@PathVariable int group_id, @AuthenticationPrincipal Jwt jwt) {

        int current_user_id = userService.findBySub(jwt.getClaimAsString("sub")).getId();
        Group group = groupService.findById(group_id);

        //creator of the group cannot leave group
        if(current_user_id == group.getGroupCreator().getId()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("The creator of the group cannot leave created group.");
        }

        groupService.deleteUserInGroup(current_user_id, group_id);
        return ResponseEntity.noContent().build();
    }

}
