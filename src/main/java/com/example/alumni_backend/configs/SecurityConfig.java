package com.example.alumni_backend.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
            .cors().and()
            .sessionManagement().disable()
            .csrf().disable()
            .authorizeHttpRequests(authorize -> authorize
                    // Allow swagger
                    .antMatchers("/api/v1/public/**","/v3/api-docs/**").permitAll()
                    //.mvcMatchers("/api/v1/adminstuff").hasRole("ADMIN")
                    .anyRequest().authenticated()
            )
            .oauth2ResourceServer()
            .jwt();
        return http.build();
    }
}
