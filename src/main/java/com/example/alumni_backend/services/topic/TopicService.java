package com.example.alumni_backend.services.topic;

import com.example.alumni_backend.models.Topic;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.CRUDService;
import org.springframework.stereotype.Service;

@Service
public interface TopicService extends CRUDService<Topic, Integer> {
    /**
     * Subscribe a user to a topic
     * @param topic_id
     * @param user_id
     */
    void newUserTopicSubscription(Integer topic_id, Integer user_id);

    /**
     * Find user in selected topic member records
     * @param user_id
     * @param topic_id
     * @return
     */
    User findUserInTopic(int user_id, int topic_id);

    /**
     * Delete user from selected topic.
     * @param user_id
     *  @param topic_id
     */
    void deleteUserInTopic(int user_id, int topic_id);
}