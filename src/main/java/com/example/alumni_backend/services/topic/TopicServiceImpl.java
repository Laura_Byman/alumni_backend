package com.example.alumni_backend.services.topic;

import com.example.alumni_backend.exceptions.EntityNotFoundException;
import com.example.alumni_backend.models.Topic;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.repositories.TopicRepository;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.services.user.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class TopicServiceImpl implements TopicService{
    private final TopicRepository topicRepository;
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    public TopicServiceImpl(TopicRepository topicRepository, UserService userService) {
        this.topicRepository = topicRepository;
        this.userService = userService;
    }

    @Override
    public Topic findById(Integer topic_id) {
        return topicRepository.findById(topic_id).orElseThrow(()-> new EntityNotFoundException(topic_id,"Topic"));
    }

    @Override
    public Collection<Topic> findAll() {
        return topicRepository.findAll();
    }

    @Override
    public Topic add(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    public Topic update(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    @Transactional
    public void deleteById(Integer topic_id) {
        if (topicRepository.existsById(topic_id)){
            topicRepository.deleteById(topic_id);
        }else {
            logger.warn("Topic with ID: " + topic_id + " not found");
        }
    }

    @Override
    public boolean exists(Integer topic_id) {
        return topicRepository.existsById(topic_id);
    }

    @Override
    public void newUserTopicSubscription(Integer topic_id, Integer user_id) {
        if(exists(topic_id) && userService.exists(user_id)){
            Topic topic = findById(topic_id);
            User user = userService.findById(user_id);
            Set<User> topicUsers = topic.getUsers()==null? new HashSet<>(): topic.getUsers();
            topicUsers.add(user);
            topic.setUsers(topicUsers);
            update(topic);
        }
    }

    @Override
    public User findUserInTopic(int user_id, int topic_id) {
        Topic topic = this.findById(topic_id);
        User user = topic.getUsers().stream().filter(u -> u.getId() == user_id).findFirst().orElse(null);
        return user;
    }

    public void deleteUserInTopic(int user_id, int topic_id){
        Topic topic = this.findById(topic_id);
        User user = this.userService.findById(user_id);

        //error checks if user not found missing
        Set<User> topicUsers = topic.getUsers();
        topicUsers.remove(user);

        topic.setUsers(topicUsers);

        this.update(topic);
    }
}
