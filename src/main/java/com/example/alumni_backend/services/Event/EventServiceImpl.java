package com.example.alumni_backend.services.Event;


import com.example.alumni_backend.exceptions.EntityNotFoundException;
import com.example.alumni_backend.models.Event;
import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.Topic;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.repositories.EventRepository;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import com.example.alumni_backend.services.user.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService{
    private final EventRepository eventRepository;
    private final GroupService groupService;
    private final UserService userService;
    private final TopicService topicService;
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    public EventServiceImpl(EventRepository eventRepository, GroupService groupService, UserService userService, TopicService topicService) {
        this.eventRepository = eventRepository;
        this.groupService = groupService;
        this.userService = userService;
        this.topicService = topicService;
    }

    @Override
    public Event findById(Integer event_id) {
        return eventRepository.findById(event_id).orElseThrow(()-> new EntityNotFoundException(event_id,"Event"));
    }
    @Override
    public Collection<Event> findAll() {
        return eventRepository.findAll();
    }
    @Override
    public Event add(Event event) {
        event.setUpdated_time(Timestamp.from(Instant.now()));
        return eventRepository.save(event);
    }
    @Override
    public Event update(Event event) {
        event.setUpdated_time(Timestamp.from(Instant.now()));
        return eventRepository.save(event);
    }
    @Override
    @Transactional
    public void deleteById(Integer event_id) {
        if (eventRepository.existsById(event_id)){
            eventRepository.deleteById(event_id);
        }else {
            logger.warn("Event with ID: " + event_id + " not found");
        }
    }
    @Override
    public boolean exists(Integer event_id) {
        return eventRepository.existsById(event_id);
    }

    @Override
    public Collection<Event> findGroupEvents(Integer user_id) {
        Collection<Integer> eventIds = eventRepository.findEventsTargetedToGroupsWhereUserIsSubscriber(user_id);
        Collection<Event> events = null;
        events = eventIds.stream().map(id -> findById(id)).collect(Collectors.toSet());
        return events;
    }
    @Override
    public Collection<Event> findTopicEvents(Integer user_id) {
        Collection<Integer> eventIds = eventRepository.findEventsTargetedToTopicsWhereUserIsSubscriber(user_id);
        Collection<Event> events = null;
        events = eventIds.stream().map(id -> findById(id)).collect(Collectors.toSet());
        return events;
    }
    @Override
    public Collection<Event> findGroupAndTopicEvents(Integer user_id) {
        Collection<Event> eventsFromGroupsAndTopics = findGroupEvents(user_id);
        findTopicEvents(user_id).stream().forEach(event -> {
            if (!eventsFromGroupsAndTopics.contains(event)) {
                eventsFromGroupsAndTopics.add(event);
            }
        });
        return eventsFromGroupsAndTopics;
    }

    @Override
    public void createRSVP(Integer event_id, Integer user_id) {
        if (userService.exists(user_id) && exists(event_id)){
            User user = userService.findById(user_id);
            Event event = findById(event_id);
            int guestCapacity = event.getGuest_capacity() == null? 0 : event.getGuest_capacity();
            int currentRSVPCount = findRSVPs(event_id).size();
            if(guestCapacity == 0 || currentRSVPCount < guestCapacity) {
                Set<User> eventUsers = event.getUsers();
                eventUsers.add(user);
                event.setUsers(eventUsers);
                update(event);
            } // RETURN THIS EVENT IS FULL
        }
    }

    @Override
    public Collection<Integer> findRSVPs(Integer event_id) {
        return eventRepository.findSpecificEventRSVPs(event_id);
    }

    @Override
    public void createGroupInvite(Integer group_id, Integer event_id) {
        if (groupService.exists(group_id) && exists(event_id)) {
            Group group = groupService.findById(group_id);
            Event event = findById(event_id);
            Set<Group> eventGroups = event.getGroups()==null? new HashSet<>():event.getGroups();
            eventGroups.add(group);
            event.setGroups(eventGroups);
            update(event);
        }
    }
    @Override
    public void deleteGroupInvite(Integer group_id, Integer event_id) {
        if (groupService.exists(group_id) && exists(event_id)){
            Event event = findById(event_id);
            Group group = groupService.findById(group_id);
            Set<Group> eventGroups = event.getGroups().stream()
                    .filter(group1 -> !group1.equals(group)).collect(Collectors.toSet());
            if (event.getGroups().size() == eventGroups.size()) logger.warn("Invitation not found");
            else {
                event.setGroups(eventGroups);
                update(event);
            }
        }
    }

    @Override
    public void createTopicInvite(Integer topic_id, Integer event_id) {
        if (topicService.exists(topic_id) && exists(event_id)){
            Event event = findById(event_id);
            Topic topic = topicService.findById(topic_id);
            Set<Topic> eventTopics = event.getTopics()==null? new HashSet<>():event.getTopics();
            eventTopics.add(topic);
            event.setTopics(eventTopics);
            update(event);
        }
    }
    @Override
    public void deleteTopicInvite(Integer topic_id, Integer event_id) {
        if (topicService.exists(topic_id) && exists(event_id)){
            Event event = findById(event_id);
            Topic topic = topicService.findById(topic_id);
            Set<Topic> eventTopics = event.getTopics().stream()
                    .filter(t -> !t.equals(topic)).collect(Collectors.toSet());
            if (event.getTopics().size() == eventTopics.size()) logger.warn("Invitation not found");
            else {
                event.setTopics(eventTopics);
                update(event);
            }
        }
    }

    @Override
    public void createUserInvite(Integer user_id, Integer event_id) {
        if (userService.exists(user_id) && exists(event_id)){
            Event event = findById(event_id);
            User user = userService.findById(user_id);
            Set<User> eventUserInvites = event.getUserInvites() == null? new HashSet<>():event.getUserInvites();
            eventUserInvites.add(user);
            event.setUserInvites(eventUserInvites);
            update(event);
        }
    }
    @Override
    public void deleteUserInvite(Integer user_id, Integer event_id) {
        if (userService.exists(user_id) && exists(event_id)){
            Event event = findById(event_id);
            User user = userService.findById(user_id);
            Set<User> eventUserInvites = event.getUserInvites().stream()
                    .filter(eui->!eui.equals(user)).collect(Collectors.toSet());
            if (event.getUserInvites().size() == eventUserInvites.size()) logger.warn("Invitation not found");
            else {
                event.setUserInvites(eventUserInvites);
                update(event);
            }
        }
    }

    @Override
    public boolean findIfUserHasInvitationForEvent(Integer user_id, Integer event_id) {
        Integer groupInvite = eventRepository.findIfUserIdIsSubscribedToGroupThatHasEventIdInvite(user_id,event_id);
        Integer topicInvite = eventRepository.findIfUserIdIsSubscribedToTopicThatHasEventIdInvite(user_id,event_id);
        if (groupInvite != null || topicInvite != null) {
            return true;
        }else return false;
    }
}