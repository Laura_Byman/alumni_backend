package com.example.alumni_backend.services.Event;

import com.example.alumni_backend.models.Event;
import com.example.alumni_backend.services.CRUDService;

import java.util.Collection;

public interface EventService extends CRUDService<Event, Integer> {
    /**
     * Get all events which have been targeted to groups that user is subscribed to.
     * @param user_id
     * @return Collection of event_id's
     */
    Collection<Event> findGroupEvents(Integer user_id);

    /**
     * Get all events which have been targeted to topics that user is subscribed to.
     * @param user_id
     * @return Collection of event_id's
     */
    Collection<Event> findTopicEvents(Integer user_id);

    /**
     * Get all events which have been targeted to groups and topics that user is subscribed to.
     * @param user_id
     * @return
     */
    Collection<Event> findGroupAndTopicEvents(Integer user_id);
    /**
     * New RSVP record
     * @param event_id
     * @param user_id
     */
    void createRSVP(Integer event_id, Integer user_id);

    /**
     * Get all RSVPs of an event
     * @param event_id
     * @return
     */
    Collection<Integer> findRSVPs(Integer event_id);

    /**
     * Create a new event group invite
     * @param group_id
     * @param event_id
     */
    void createGroupInvite(Integer group_id, Integer event_id);
    /**
     * Delete a group invite. Does not delete RSVP records.
     * @param group_id
     * @param event_id
     */
    void deleteGroupInvite(Integer group_id, Integer event_id);

    /**
     * Create a new event topic invite
     * @param topic_id
     * @param event_id
     */
    void createTopicInvite(Integer topic_id, Integer event_id);
    /**
     * Delete a topic invite. Does not delete RSVP records.
     * @param topic_id
     * @param event_id
     */
    void deleteTopicInvite(Integer topic_id, Integer event_id);

    /**
     * Create a new event user invite
     * @param user_id
     * @param event_id
     */
    void createUserInvite(Integer user_id, Integer event_id);
    /**
     * Delete a user invite. Does not delete RSVP records.
     * @param user_id
     * @param event_id
     */
    void deleteUserInvite(Integer user_id, Integer event_id);

    /**
     *  Check if user has invitation for a specific event
     * @param user_id
     * @param event_id
     * @return
     */
    boolean findIfUserHasInvitationForEvent(Integer user_id, Integer event_id);
}