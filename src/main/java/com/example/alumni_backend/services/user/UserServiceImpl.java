package com.example.alumni_backend.services.user;

import com.example.alumni_backend.exceptions.EntityNotFoundException;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(Integer user_id) {
        return userRepository.findById(user_id).orElseThrow(() -> new EntityNotFoundException(user_id, "User"));
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User add(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public void deleteById(Integer user_id) {
        if (userRepository.existsById(user_id)){
            userRepository.deleteById(user_id);
        }else logger.warn("User with ID: " + user_id + " not found");
    }
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public Set<User> findAllByName(String name) {
        return userRepository.findAllByName(name);
    }

    @Override
    public Set<User> findAllById(Set<Integer> userIds) {
        Set<User> users = null;
        if (userIds != null){
            users = userIds.stream().map(id -> findById(id)).collect(Collectors.toSet());
        }
        return users;
    }

    @Override
    public User findBySub(String sub) {
        return userRepository.findBySub(sub);
    }

    @Override
    public boolean exists(Integer id) {
        return userRepository.existsById(id);
    }
}
