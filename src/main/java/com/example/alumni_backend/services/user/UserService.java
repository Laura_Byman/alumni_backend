package com.example.alumni_backend.services.user;

import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.CRUDService;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface UserService extends CRUDService<User, Integer> {
    Set<User> findAllByName(String name);
    Set<User> findAllById(Set<Integer> userIds);

    /**
     * Find user by keycloak assigned unique id: sub
     * @param sub
     * @return User object
     */
    User findBySub(String sub);
}
