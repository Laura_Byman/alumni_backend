package com.example.alumni_backend.services.group;


import com.example.alumni_backend.exceptions.EntityNotFoundException;
import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.repositories.GroupRepository;
import com.example.alumni_backend.services.user.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {
    private final Logger logger = LoggerFactory.getLogger(com.example.alumni_backend.services.group.GroupServiceImpl.class);
    private final GroupRepository groupRepository;
    private final UserService userService;

    public GroupServiceImpl(GroupRepository groupRepository, UserService userService) {
        this.groupRepository = groupRepository;
        this.userService = userService;
    }

    @Override
    public Group findById(Integer id) {
        return groupRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id, "Group"));
    }

    @Override
    public Collection<Group> findAll() {
        return groupRepository.findAll();
    }

    //find all public groups and those private groups requesting user is a member of.
    @Override
    public Collection<Group> findAllExcludePrivateGroupsThatUserIsNotMember(int user_id) {
        User user = this.userService.findById(user_id);

        Set<Group> groupsExclPrivateUserIsNotMemb = groupRepository.findAll().stream()
                .filter(g -> g.is_private() == false || this.findUserInGroup(user.getId(), g.getId()) != null).collect(Collectors.toSet());

        return groupsExclPrivateUserIsNotMemb;
    }

    @Override
    public Group add(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group update(Group group) {
        return groupRepository.save(group);
    }

    @Override
    @Transactional
    public void deleteById(Integer group_id) {
        if (groupRepository.existsById(group_id)){
            groupRepository.deleteById(group_id);
        }else {
            logger.warn("Group with ID: " + group_id + " not found");
        }
    }

    //user join to a existing group
    @Override
    public void addUserToGroup(int user_id, int group_id){
        Group group = this.findById(group_id);
        User user = this.userService.findById(user_id);

        Set<User> groupUsers = group.getUsers();
        groupUsers.add(user);
        group.setUsers(groupUsers);

        this.update(group);
    }

    @Override
    public void deleteUserInGroup(int user_id, int group_id){
        Group group = this.findById(group_id);
        User user = this.userService.findById(user_id);

        //error checks if user not found missing
        Set<User> groupUsers = group.getUsers();
        groupUsers.remove(user);

        group.setUsers(groupUsers);

        this.update(group);
    }

    @Override
    public User findUserInGroup(int user_id, int group_id){
        Group group = this.findById(group_id);

        User user = group.getUsers().stream().filter(u -> u.getId() == user_id).findFirst().orElse(null);;

        return user;
    }

    @Override
    public User findUserGroupJoinRequest(int user_id, int group_id){
        Group group = this.findById(group_id);

        User user = group.getUserJoinRequests().stream().filter(u -> u.getId() == user_id).findFirst().orElse(null);;

        return user;
    }

    @Override
    public  void createGroupJoinRequest(int user_id, int group_id) {
        Group group = this.findById(group_id);
        User user = userService.findById(user_id);

        Set<User> groupUserJoinRequests = group.getUserJoinRequests();
        groupUserJoinRequests.add(user);
        group.setUserJoinRequests(groupUserJoinRequests);

        this.update(group);
    }

    @Override
    public void deleteUserGroupJoinRequest(int user_id, int group_id){
        Group group = this.findById(group_id);
        User user = this.userService.findById(user_id);

        Set<User> groupUserJoinRequests = group.getUserJoinRequests();
        groupUserJoinRequests.remove(user);

        group.setUserJoinRequests(groupUserJoinRequests);

        this.update(group);
    }

    @Override
    public boolean exists(Integer id) {
        return groupRepository.existsById(id);
    }
}