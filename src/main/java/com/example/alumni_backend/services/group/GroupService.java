package com.example.alumni_backend.services.group;

import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.CRUDService;

import java.util.Collection;

public interface GroupService extends CRUDService<Group, Integer> {
    /**
     * Add user as a member of selected group.
     * @param user_id
     *  @param group_id
     */
    void addUserToGroup(int user_id, int group_id);

    /**
     * Delete user from selected group.
     * @param user_id
     *  @param group_id
     */
    void deleteUserInGroup(int user_id, int group_id);

    /**
     * Find user in selected group member records
     * @param user_id
     *  @param group_id
     * @return User object
     */
    User findUserInGroup(int user_id, int group_id);

    /**
     * Return a collection of all public groups and of those private groups the requesting user is a member of.
     * Excluded are those private groups the user does not have membership
     * @param user_id
     * @return Collection of Group objects
     */
    Collection<Group>  findAllExcludePrivateGroupsThatUserIsNotMember(int user_id);

    /**
     * Find users join request in selected group open join requests
     * @param user_id
     *  @param group_id
     * @return User object
     */
    public User findUserGroupJoinRequest(int user_id, int group_id);

    /**
     * Crete new open request for user to join selected group.
     * @param user_id
     *  @param group_id
     */
    void createGroupJoinRequest(int user_id, int group_id);

    /**
     * Delete users group join request.
     * @param user_id
     *  @param group_id
     */
    void deleteUserGroupJoinRequest(int user_id, int group_id);
}