package com.example.alumni_backend.services.post;

import com.example.alumni_backend.exceptions.EntityNotFoundException;
import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.repositories.PostRepository;
import com.example.alumni_backend.services.Event.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
    private final EventService eventService;

    public PostServiceImpl(PostRepository postRepository, EventService eventService) {

        this.postRepository = postRepository;
        this.eventService = eventService;
    }

    @Override
    public Post findById(Integer id) {
        return postRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id, "Post"));
    }

    @Override
    public Collection<Post> findAll() {
        List<Post> posts = postRepository.findAll();
        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }

    @Override
    public Post add(Post entity) {

        //change updated_time to current time
        entity.setCreated_time(new Timestamp(System.currentTimeMillis()));
        entity.setUpdated_time(new Timestamp(System.currentTimeMillis()));

        return postRepository.save(entity);
    }

    @Override
    public Post update(Post entity) {

        //change updated_time to current time
        entity.setUpdated_time(new Timestamp(System.currentTimeMillis()));
        return postRepository.save(entity);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if(postRepository.existsById(id)) {
            Post post = postRepository.findById(id).get();
            postRepository.delete(post);
        }
        else logger.warn("No post exists with ID: " + id);
    }

    @Override
    public boolean exists(Integer id) {
        return postRepository.existsById(id);
    }

    @Override
    public Collection<Post> findAllByGroup(int group_id){
        List<Post> posts = postRepository.findAllByGroup(group_id);
        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }

    @Override
    public Collection<Post> findAllByTopic(int topic_id){
        List<Post> posts = postRepository.findAllByTopic(topic_id);
        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }

    @Override
    public Collection<Post> findAllByEvent(int event_id){
        List<Post> posts = postRepository.findAllByEvent(event_id);
        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }

    @Override
    public Collection<Post> findAllByUserGroupSubscription(int user_id){
        List<Post> posts = postRepository.findAllByUserGroupSubscription(user_id);

        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }

    @Override
    public Collection<Post> findAllByUserTopicSubscription(int user_id){
        List<Post> posts = postRepository.findAllByUserTopicSubscription(user_id);

        Collections.sort(posts, (p1, p2) -> p1.getUpdated_time().compareTo(p2.getUpdated_time()));

        return posts;
    }
}