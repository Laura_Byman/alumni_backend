package com.example.alumni_backend.services.post;

import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.services.CRUDService;

import java.util.Collection;
import java.util.List;

public interface PostService extends CRUDService<Post, Integer> {
    /**
     * Find all posts posted to selected group
     *  @param group_id
     * @return Collection of Post objects
     */
    Collection<Post> findAllByGroup(int group_id);

    /**
     * Find all posts posted to selected topic
     *  @param topic_id
     * @return Collection of Post objects
     */
    Collection<Post> findAllByTopic(int topic_id);

    /**
     * Find all posts posted to selected event
     *  @param event_id
     * @return Collection of Post objects
     */
    Collection<Post> findAllByEvent(int event_id);

    /**
     * Find all posts of all groups the user has subscribed.
     *  @param user_id
     * @return Collection of Post objects
     */
    Collection<Post> findAllByUserGroupSubscription(int user_id);

    /**
     * Find all posts of all topics the user has subscribed.
     *  @param user_id
     * @return Collection of Post objects
     */
    Collection<Post> findAllByUserTopicSubscription(int user_id);
}