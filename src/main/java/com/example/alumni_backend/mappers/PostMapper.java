package com.example.alumni_backend.mappers;

import com.example.alumni_backend.models.*;
import com.example.alumni_backend.models.DTOs.NewPostDTO;
import com.example.alumni_backend.models.DTOs.PostDTO;
import com.example.alumni_backend.services.Event.EventService;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.post.PostService;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class PostMapper {
    @Autowired
    protected PostService postService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected GroupService groupService;
    @Autowired
    protected EventService eventService;
    @Autowired
    protected TopicService topicService;

    @Mapping(target = "original_post_id", source = "original_post.id")
    @Mapping(target = "target_group_id", source = "target_group.id")
    @Mapping(target = "target_topic_id", source = "target_topic.id")
    @Mapping(target = "target_event_id", source = "target_event.id")
    @Mapping(target = "author", source = "author.id")
    @Mapping(target = "reply_posts", source = "reply_posts", qualifiedByName = "replyPostsToIds")
    public abstract PostDTO postToPostDto(Post post);

    public abstract Collection<PostDTO> postToPostDto(Collection<Post> posts);

    @Mapping(target = "author", source = "author", qualifiedByName = "authorIdToUser")
    @Mapping(target = "original_post", source = "original_post_id", qualifiedByName = "postIdToPost")
    @Mapping(target = "target_group", source = "target_group_id", qualifiedByName = "groupIdToGroup")
    @Mapping(target = "target_topic", source = "target_topic_id", qualifiedByName = "TopicIdToTopicObject")
    @Mapping(target = "target_event", source = "target_event_id", qualifiedByName = "eventIdToEvent")
    @Mapping(target = "reply_posts", source = "reply_posts", qualifiedByName = "replyPostIdsToPosts")
    public abstract Post postDtoToPost(PostDTO dto);

    @Mapping(target = "author", source = "author", qualifiedByName = "authorIdToUser")
    @Mapping(target = "original_post", source = "original_post_id", qualifiedByName = "postIdToPost")
    @Mapping(target = "target_group", source = "target_group_id", qualifiedByName = "groupIdToGroup")
    @Mapping(target = "target_topic", source = "target_topic_id", qualifiedByName = "TopicIdToTopicObject")
    @Mapping(target = "target_event", source = "target_event_id", qualifiedByName = "eventIdToEvent")
    @Mapping(target = "reply_posts", ignore = true)
    public abstract Post newPostDtoToPost(NewPostDTO newPostDTO);

    @Named("postIdToPost")
    Post mapIdToPost(int id) {
        System.out.println(id);
        if(id == 0){
            return null;
        }
        return postService.findById(id);
    }

    @Named("groupIdToGroup")
    Group mapIdToGroup(int id) {
        System.out.println(id);
        if(id == 0){
            return null;
        }
        return groupService.findById(id);
    }

    @Named("TopicIdToTopicObject")
    Topic mapIdToTopic(int id) {
        System.out.println(id);
        if(id == 0){
            return null;
        }
        return topicService.findById(id);
    }

    @Named("eventIdToEvent")
    Event mapIdToEvent(int id) {
        System.out.println(id);
        if(id == 0){
            return null;
        }
        return eventService.findById(id);
    }

    @Named("authorIdToUser")
    User authorIdToUser(int id) {
        return userService.findById(id);
    }

    @Named("replyPostIdsToPosts")
    Set<Post> mapIdsToPosts(Set<Integer> id) {
        return id.stream()
                .map( i -> postService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("replyPostsToIds")
    Set<Integer> mapPostsToIds(Set<Post> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
