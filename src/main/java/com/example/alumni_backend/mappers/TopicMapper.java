package com.example.alumni_backend.mappers;

import com.example.alumni_backend.models.DTOs.NewTopicDTO;
import com.example.alumni_backend.models.DTOs.TopicDTO;
import com.example.alumni_backend.models.Event;
import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.models.Topic;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.topic.TopicService;
import com.example.alumni_backend.services.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class TopicMapper {
    @Autowired
    TopicService topicService;
    @Autowired
    protected UserService userService;

    //public abstract Topic topicDTOToTopic(TopicDTO topicDTO);

    @Mapping(target = "events", source = "events", qualifiedByName = "eventsToIds")
    @Mapping(target = "topic_posts", source = "topic_posts", qualifiedByName = "postsToIds")
    @Mapping(target = "users", source = "users", qualifiedByName = "usersToIds")
    @Mapping(target = "topicCreator", source = "topicCreator.id")
    public abstract TopicDTO topicToTopicDTO(Topic topic);
    public abstract Collection<TopicDTO> topicToTopicDTO(Collection<Topic> topics);

    @Mapping(target = "events",ignore = true)
    @Mapping(target = "topicCreator", source = "topicCreator", qualifiedByName = "userIdToUser")
    @Mapping(target = "topic_posts",ignore = true)
    @Mapping(target = "users",ignore = true)
    @Mapping(target = "id",ignore = true)
    public abstract Topic newTopicDTOToTopic(NewTopicDTO newTopicDTO);

    @Named("eventsToIds")
    Set<Integer> mapEventsToIds(Collection<Event> events){
        if (events != null){
            return events.stream().map(event -> event.getId()).collect(Collectors.toSet());
        }
        return null;
    }

    @Named("postsToIds")
    Set<Integer> mapPostsToIds(Collection<Post> posts){
        if (posts != null){
            return posts.stream().map(post -> post.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("usersToIds")
    Set<Integer> mapUsersToIds(Collection<User> users){
        if (users != null){
            return users.stream().map(user -> user.getId()).collect(Collectors.toSet());
        }
        return null;
    }

    @Named("userIdToUser")
    User mapIdToUser(int id) {
        if(id == 0){
            return null;
        }
        return userService.findById(id);
    }

}
