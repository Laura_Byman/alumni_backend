package com.example.alumni_backend.mappers;

import com.example.alumni_backend.models.*;
import com.example.alumni_backend.models.DTOs.PublicUserDTO;
import com.example.alumni_backend.models.DTOs.UpdateUserDTO;
import com.example.alumni_backend.models.DTOs.UserDTO;
import com.example.alumni_backend.services.user.UserService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    @Autowired
    UserService userService;

    @Mapping(target = "events", source = "events", qualifiedByName = "eventsToIds")
    @Mapping(target = "groups", source = "groups", qualifiedByName = "groupsToIds")
    @Mapping(target = "ownPosts", source = "ownPosts", qualifiedByName = "postsToIds")
    @Mapping(target = "ownGroups", source = "ownGroups", qualifiedByName = "groupsToIds")
    @Mapping(target = "ownTopics", source = "ownTopics", qualifiedByName = "topicsToIds")
    @Mapping(target = "groupJoinRequests", source = "groupJoinRequests", qualifiedByName = "groupsToIds")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "topicsToIds")
    @Mapping(target = "eventInvites", source = "eventInvites", qualifiedByName = "eventsToIds")
    public abstract UserDTO userToUserDTO(User user);
    @Mapping(target = "events", source = "events", qualifiedByName = "eventsToIds")
    @Mapping(target = "groups", source = "groups", qualifiedByName = "groupsToIds")
    public abstract PublicUserDTO userToPublicUserDTO(User user);
    public abstract Collection<PublicUserDTO> userToPublicUserDTO(Collection<User> user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sub", ignore = true)
    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "ownPosts", ignore = true)
    @Mapping(target = "ownGroups", ignore = true)
    @Mapping(target = "ownTopics", ignore = true)
    @Mapping(target = "groupJoinRequests", ignore = true)
    @Mapping(target = "events", ignore = true)
    @Mapping(target = "topics", ignore = true)
    @Mapping(target = "eventInvites", ignore = true)
    public abstract User userDTOToUser(UserDTO userDTO);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sub", ignore = true)
    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "ownPosts", ignore = true)
    @Mapping(target = "ownGroups", ignore = true)
    @Mapping(target = "ownTopics", ignore = true)
    @Mapping(target = "groupJoinRequests", ignore = true)
    @Mapping(target = "events", ignore = true)
    @Mapping(target = "topics", ignore = true)
    @Mapping(target = "eventInvites", ignore = true)
    public abstract User updateUserDTOToUser(UpdateUserDTO updateUserDTO);

    @Named("eventsToIds")
    Set<Integer> mapEventsToIds(Collection<Event> events){
        if (events != null){
            return events.stream().map(event -> event.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("groupsToIds")
    Set<Integer> mapGroupsToIds(Collection<Group> groups){
        if (groups != null){
            return groups.stream().map(group -> group.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("topicsToIds")
    Set<Integer> mapTopicsToIds(Collection<Topic> topics){
        if (topics != null){
            return topics.stream().map(topic -> topic.getId()).collect(Collectors.toSet());
        }
        return null;
    }

    @Named("postsToIds")
    Set<Integer> mapPostsToIds(Collection<Post> posts){
        if (posts != null){
            return posts.stream().map(post -> post.getId()).collect(Collectors.toSet());
        }
        return null;
    }
}
