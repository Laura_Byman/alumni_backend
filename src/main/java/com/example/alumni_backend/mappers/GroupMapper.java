package com.example.alumni_backend.mappers;

import com.example.alumni_backend.models.DTOs.GroupDTO;
import com.example.alumni_backend.models.DTOs.NewGroupDTO;
import com.example.alumni_backend.models.Event;
import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.models.User;
import com.example.alumni_backend.services.group.GroupService;
import com.example.alumni_backend.services.post.PostService;
import com.example.alumni_backend.services.user.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class GroupMapper {
    @Autowired
    protected GroupService groupService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected PostService postService;


    @Mapping(target = "group_posts", source = "group_posts", qualifiedByName = "groupPostsToIds")
    @Mapping(target = "users", source = "users", qualifiedByName = "usersToIds")
    @Mapping(target = "groupCreator", source = "groupCreator.id")
    @Mapping(target = "userJoinRequests", source = "userJoinRequests", qualifiedByName = "usersToIds")
    @Mapping(target = "events", source = "events", qualifiedByName = "eventsToIds")
    public abstract GroupDTO groupToGroupDto(Group group);


    public abstract Collection<GroupDTO> groupToGroupDto(Collection<Group> groups);

    @Mapping(target = "group_posts", source = "group_posts", qualifiedByName = "groupPostIdsToPosts")
    @Mapping(target = "users", source = "users", qualifiedByName = "userIdsToUsers")
    @Mapping(target = "userJoinRequests", source = "userJoinRequests", qualifiedByName = "userIdsToUsers")
    @Mapping(target = "groupCreator", source = "groupCreator", qualifiedByName = "userIdToUser")
    @Mapping(target = "events", ignore = true)
    public abstract Group groupDtoToGroup(GroupDTO dto);

    @Mapping(target = "users", source = "users", qualifiedByName = "userIdsToUsers")
    @Mapping(target = "group_posts", ignore = true)
    @Mapping(target = "groupCreator", source = "groupCreator", qualifiedByName = "userIdToUser")
    @Mapping(target = "userJoinRequests", ignore = true)
    @Mapping(target = "events", ignore = true)
    public abstract Group newGroupDtoToGroup(NewGroupDTO dto);

    @Named("groupPostIdsToPosts")
    Set<Post> mapIdsToPost(Set<Integer> id) {
        return id.stream()
                .map( i -> postService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("groupPostsToIds")
    Set<Integer> mapPostsToIds(Set<Post> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("userIdsToUsers")
    Set<User> mapIdsToUser(Set<Integer> id) {
        return id.stream()
                .map( i -> userService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("usersToIds")
    Set<Integer> mapUsersToIds(Set<User> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("eventsToIds")
    Set<Integer> mapEventsToIds(Set<Event> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("userIdToUser")
    User mapIdToUser(int id) {
        if(id == 0){
            return null;
        }
        return userService.findById(id);
    }

}
