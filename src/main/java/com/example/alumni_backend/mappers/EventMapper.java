package com.example.alumni_backend.mappers;

import com.example.alumni_backend.models.*;
import com.example.alumni_backend.models.DTOs.EventDTO;
import com.example.alumni_backend.models.DTOs.NewEventDTO;
import com.example.alumni_backend.services.Event.EventService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class EventMapper {
    @Autowired
    EventService eventService;

    @Mapping(target = "posts", source = "posts", qualifiedByName = "postsToIds")
    @Mapping(target = "groups", source = "groups", qualifiedByName = "groupsToIds")
    @Mapping(target = "users", source = "users", qualifiedByName = "usersToIds")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "topicsToIds")
    @Mapping(target = "userInvites", source = "userInvites", qualifiedByName = "usersToIds")
    public abstract EventDTO eventToEventDTO(Event event);
    public abstract Collection<EventDTO> eventToEventDTO(Collection<Event> event);
    @Mapping(target = "id",ignore = true)
    @Mapping(target = "host",ignore = true)
    @Mapping(target = "posts",ignore = true)
    @Mapping(target = "groups",ignore = true)
    @Mapping(target = "users",ignore = true)
    @Mapping(target = "topics",ignore = true)
    @Mapping(target = "userInvites", ignore = true)
    @Mapping(target = "updated_time",ignore = true)
    public abstract Event newEventDTOToEvent(NewEventDTO newEventDTO);

    @Named("postsToIds")
    Set<Integer> mapPostsToIds(Collection<Post> posts){
        if (posts != null){
            return posts.stream().map(post -> post.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("groupsToIds")
    Set<Integer> mapGroupsToIds(Collection<Group> groups){
        if (groups != null){
            return groups.stream().map(group -> group.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("usersToIds")
    Set<Integer> mapUsersToIds(Collection<User> users){
        if (users != null) {
            return users.stream().map(user -> user.getId()).collect(Collectors.toSet());
        }
        return null;
    }
    @Named("topicsToIds")
    Set<Integer> mapTopicsToIds(Collection<Topic> topics){
        if (topics != null){
            return topics.stream().map(topic -> topic.getId()).collect(Collectors.toSet());
        }
        return null;
    }
}
