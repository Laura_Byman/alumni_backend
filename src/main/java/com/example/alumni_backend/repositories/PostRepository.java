package com.example.alumni_backend.repositories;

import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.Post;
import com.example.alumni_backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    @Query("SELECT post FROM Post post WHERE post.target_group.id = :group_id")
    List<Post> findAllByGroup(int group_id);

    @Query("SELECT post FROM Post post WHERE post.target_topic.id = :topic_id")
    List<Post> findAllByTopic(int topic_id);

    @Query("SELECT post FROM Post post WHERE post.target_event.id = :event_id")
    List<Post> findAllByEvent(int event_id);

    @Query("SELECT post FROM Post post JOIN post.target_group g JOIN g.users u WHERE u.id = :user_id")
    List<Post> findAllByUserGroupSubscription(int user_id);

    @Query("SELECT post FROM Post post JOIN post.target_topic g JOIN g.users u WHERE u.id = :user_id")
    List<Post> findAllByUserTopicSubscription(int user_id);

}