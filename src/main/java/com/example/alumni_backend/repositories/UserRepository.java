package com.example.alumni_backend.repositories;

import com.example.alumni_backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("SELECT u FROM User u WHERE u.name LIKE %?1%")
    Set<User> findAllByName(String name);

    /**
     * Find user by sub (keycloak provided id)
     * @param sub
     * @return User object
     */
    User findBySub(String sub);
}
