package com.example.alumni_backend.repositories;

import com.example.alumni_backend.models.Group;
import com.example.alumni_backend.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {
}
