package com.example.alumni_backend.repositories;

import com.example.alumni_backend.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    /**
     * Get all events which have been targeted to groups that user is subscribed to.
     * @param user_id
     * @return Collection of event_id's
     */
    @Query(value = "SELECT DISTINCT \"event\".id "
            + "FROM \"event\" "
            + "INNER JOIN event_group_invite AS egi ON \"event\".id = egi.event_id "
            + "INNER JOIN \"group\" ON egi.group_id = \"group\".id "
            + "INNER JOIN group_user AS ug ON \"group\".id = ug.group_id "
            + "INNER JOIN \"user\" ON ug.user_id = \"user\".id "
            + "WHERE user_id=?",
            nativeQuery = true)
    Collection<Integer> findEventsTargetedToGroupsWhereUserIsSubscriber(Integer user_id);

    /**
     * Get all events which have been targeted to topics that user is subscribed to.
     * @param user_id
     * @return
     */
    @Query(value = "SELECT DISTINCT \"event\".id "
            + "FROM \"event\" "
            + "INNER JOIN event_topic_invite AS eti ON \"event\".id = eti.event_id "
            + "INNER JOIN topic ON eti.topic_id = topic.id "
            + "INNER JOIN topic_user AS tu ON topic.id = tu.topic_id "
            + "INNER JOIN \"user\" ON tu.user_id = \"user\".id "
            + "WHERE user_id=?",
            nativeQuery = true)
    Collection<Integer> findEventsTargetedToTopicsWhereUserIsSubscriber(Integer user_id);

    /**
     * Search for distinct user id linked by user-group-event.
     * @param user_id
     * @param event_id
     * @return
     */
    @Query(value = "SELECT DISTINCT u.id FROM User u JOIN u.groups g JOIN g.events e WHERE user_id=?1 AND event_id=?2")
    Integer findIfUserIdIsSubscribedToGroupThatHasEventIdInvite(Integer user_id, Integer event_id);

    /**
     * Search for distinct user id linked by user-topic-event.
     * @param user_id
     * @param event_id
     * @return
     */
    @Query(value = "SELECT DISTINCT u.id FROM User u JOIN u.topics t JOIN t.events e WHERE user_id=?1 AND event_id=?2")
    Integer findIfUserIdIsSubscribedToTopicThatHasEventIdInvite(Integer user_id, Integer event_id);

    /**
     * Get all RSVPs of a given event
     * @param event_id
     * @return
     */
    @Query(value = "SELECT DISTINCT u.id FROM User u JOIN u.events e WHERE event_id=?1")
    Collection<Integer> findSpecificEventRSVPs(Integer event_id);
}