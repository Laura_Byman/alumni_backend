package com.example.alumni_backend.repositories;

import com.example.alumni_backend.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TopicRepository extends JpaRepository<Topic, Integer> {
}
