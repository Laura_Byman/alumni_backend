package com.example.alumni_backend.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="`event`")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 150,nullable = false)
    private String title;
    @Column(length = 4000)
    private String description;
    @Column(nullable = false)
    private int host;
    @Column
    private Timestamp date_time_begin;
    @Column
    private Timestamp date_time_end;
    @Column(nullable = false)
    private Timestamp updated_time;
    @Column
    private Integer guest_capacity;
    @OneToMany(mappedBy = "target_event")
    private Collection<Post> posts;
    @ManyToMany
    @JoinTable(
            name = "event_group_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    private Set<Group> groups;
    @ManyToMany
    @JoinTable(
            name = "rsvp",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> users;
    @ManyToMany
    @JoinTable(
            name = "event_topic_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id")}
    )
    private Set<Topic> topics;
    @ManyToMany
    @JoinTable(
            name = "event_user_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> userInvites;
}