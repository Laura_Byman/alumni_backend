package com.example.alumni_backend.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 150,nullable = false)
    private String title;
    @Column(length = 4000)
    private String description;
    @ManyToMany(mappedBy = "topics")
    private Set<Event> events;
    @OneToMany(mappedBy = "target_topic")
    private Set<Post> topic_posts;
    @ManyToOne
    @JoinColumn(name = "topic_creator", nullable = false)
    private User topicCreator;
    @ManyToMany
    @JoinTable(
            name = "topic_user",
            joinColumns = {@JoinColumn(name = "topic_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> users;
}
