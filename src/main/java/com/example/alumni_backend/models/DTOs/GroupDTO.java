package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class GroupDTO {
    private int id;
    private String title;
    private String description;
    private boolean is_private;
    private int groupCreator;
    private Set<Integer> users;
    private Set<Integer> userJoinRequests;
    private Set<Integer> group_posts;
    private Set<Integer> events;
}
