package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class TopicDTO {
    private int id;
    private String title;
    private String description;
    private int topicCreator;
    private Set<Integer> events;
    private Set<Integer> topic_posts;
    private Collection<Integer> users;
}
