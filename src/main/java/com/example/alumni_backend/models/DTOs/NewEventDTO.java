package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class NewEventDTO {
    private String title;
    private String description;
    private Timestamp date_time_begin;
    private Timestamp date_time_end;
    private Integer guest_capacity;
}
