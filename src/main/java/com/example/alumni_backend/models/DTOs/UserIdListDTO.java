package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UserIdListDTO {
    private Set<Integer> userIds;
}
