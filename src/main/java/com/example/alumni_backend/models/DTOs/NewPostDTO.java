package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class NewPostDTO {
    private int id;
    private String title;
    private String body;
    private Timestamp created_time;
    private Timestamp updated_time;
    private int author;
    private int target_group_id;
    private int target_topic_id;
    private int target_event_id;
    private int original_post_id;
}
