package com.example.alumni_backend.models.DTOs;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UserDTO {
    private int id;
    private String name;
    private String sub;
    private String avatar;
    private String status_message;
    private String bio;
    private String fun_fact;
    private Collection<Integer> groups;
    private Collection<Integer> ownPosts;
    private Collection<Integer> ownGroups;
    private Collection<Integer> ownTopics;
    private Collection<Integer>  groupJoinRequests;
    private Collection<Integer> events;
    private Collection<Integer> topics;
    private Collection<Integer> eventInvites;
}
