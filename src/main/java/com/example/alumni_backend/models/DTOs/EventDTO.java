package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class EventDTO {
    private int id;
    private String title;
    private String description;
    private int host;
    private Timestamp date_time_begin;
    private Timestamp date_time_end;
    private Timestamp updated_time;
    private Integer guest_capacity;
    private Collection<Integer> posts;
    private Set<Integer> groups;
    private Set<Integer> users;
    private Set<Integer> topics;
    private Set<Integer> userInvites;

}
