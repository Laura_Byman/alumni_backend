package com.example.alumni_backend.models.DTOs;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class NewTopicDTO {
    private String title;
    private String description;
    private int topicCreator;
    private Set<Integer> users;
}

