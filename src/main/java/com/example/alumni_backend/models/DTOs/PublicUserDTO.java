package com.example.alumni_backend.models.DTOs;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PublicUserDTO {
    private String id;
    private String name;
    private String avatar;
    private String status_message;
    private String bio;
    private String fun_fact;
    private Collection<Integer> groups;
    private Collection<Integer> events;
}