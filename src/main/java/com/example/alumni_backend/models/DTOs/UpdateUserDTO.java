package com.example.alumni_backend.models.DTOs;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UpdateUserDTO {
    private String name;
    private String avatar;
    private String status_message;
    private String bio;
    private String fun_fact;
}
