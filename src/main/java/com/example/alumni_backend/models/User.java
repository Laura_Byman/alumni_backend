package com.example.alumni_backend.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="`user`")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 150,nullable = false)
    private String name;
    @Column
    private String sub;
    @Column(length = 300)
    private String avatar;
    @Column(length = 300)
    private String status_message;
    @Column(length = 4000)
    private String bio;
    @Column(length = 500)
    private String fun_fact;
    @OneToMany(mappedBy = "author")
    private Set<Post> ownPosts;
    @OneToMany(mappedBy = "groupCreator")
    private Set<Group> ownGroups;
    @OneToMany(mappedBy = "topicCreator")
    private Set<Topic> ownTopics;
    @ManyToMany(mappedBy = "users")
    private Set<Group> groups;
    @ManyToMany(mappedBy = "userJoinRequests")
    private Set<Group> groupJoinRequests;
    @ManyToMany(mappedBy = "users")
    private Set<Event> events;
    @ManyToMany(mappedBy = "users")
    private Set<Topic> topics;
    @ManyToMany(mappedBy = "userInvites")
    private Set<Event> eventInvites;
}
