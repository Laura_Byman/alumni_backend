package com.example.alumni_backend.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Getter
@Setter

public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 150, nullable = false)
    private String title;
    @Column(nullable = false)
    private String body;
    @Column(nullable = false)
    private Timestamp created_time;
    @Column(nullable = false)
    private Timestamp updated_time;
    @ManyToOne
    @JoinColumn(name = "original_post")
    private Post original_post;
    @OneToMany(mappedBy = "original_post")
    private Set<Post> reply_posts;
    @ManyToOne
    @JoinColumn(name = "author", nullable = false)
    private User author;
    @ManyToOne
    @JoinColumn(name = "target_group")
    private Group target_group;
    @ManyToOne
    @JoinColumn(name = "target_topic")
    private Topic target_topic;
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event target_event;
}
