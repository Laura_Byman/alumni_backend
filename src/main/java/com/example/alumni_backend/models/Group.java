package com.example.alumni_backend.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="`group`")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 150, nullable = false)
    private String title;
    @Column(length = 4000, nullable = false)
    private String description;
    @Column(nullable = false)
    private boolean is_private;
    @ManyToOne
    @JoinColumn(name = "group_creator", nullable = false)
    private User groupCreator;
    @OneToMany(mappedBy = "target_group")
    private Set<Post> group_posts;
    @ManyToMany
    @JoinTable(
            name = "group_user",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> users;
    @ManyToMany
    @JoinTable(
            name = "group_user_join_request",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> userJoinRequests;
    @ManyToMany(mappedBy = "groups")
    private Set<Event> events;
}
