INSERT INTO "user" ("name",avatar,status_message,bio,fun_fact, sub) VALUES ('jani','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png','working','fullstack developer','facts are not funny','51f9d2ac-5ada-4dd8-af6a-08bd397a2619');
INSERT INTO "user" ("name",status_message,bio,fun_fact, sub) VALUES ('john','vanishing','anonymous actor','not so anonymous irl', '55f917cf-b4b4-43b3-b0f6-7e613aa97caf');
INSERT INTO "user" ("name",status_message,bio,fun_fact, sub) VALUES ('joe','conjuring','next level wizard','has met gandalf','64297d29-6407-437e-bbca-c9fef842aca6');
INSERT INTO "user" ("name",status_message,bio,fun_fact) VALUES ('jesse','cooking','cooking with walter since 2008','crystal clear');
INSERT INTO "user" ("name",status_message,bio,fun_fact) VALUES ('josh','chilax','nothing much','can cook 1 minute noodles in 50 sec');

INSERT INTO "group" (title, description, is_private, group_creator) VALUES ('Experis Alumni 2022','A group for all Experis alumnis from 2022',false, 2);
INSERT INTO "group" (title,description,is_private, group_creator) VALUES ('Experis Alumni 2021','A group for all Experis alumnis from 2021',false, 2);
INSERT INTO "group" (title,description,is_private, group_creator) VALUES ('Experis alumnis of Finland','A private group for finnish Experis alumnis',true, 1);

INSERT INTO "event" (title, description, host, updated_time, date_time_begin) VALUES ('Evening coffee', 'Coffee moment in the evening', 1,'2022-06-22 19:10:25-07', '2022-10-22 19:10:25-07');
INSERT INTO "event" (title, description, host, guest_capacity, updated_time, date_time_begin) VALUES ('Afternoon tea', 'Tea time just like the English do', 2,10,'2022-10-22 19:10:25-07', '2022-10-22 19:10:25-07');
INSERT INTO "event" (title, description, host, updated_time, date_time_begin) VALUES ('Finnish breakfast', 'Fifth of vodka, cigarette, painkiller', 3,'2022-10-20 19:10:25-07', '2022-10-22 19:10:25-07');
INSERT INTO "event" (title,description, host, guest_capacity, updated_time, date_time_begin) VALUES ('Finnish Alumnis meeting 2022','A finnish Experis alumnis meeting',3, 50, '2022-07-1 10:10:10-01', '2022-10-25 19:10:25-07');

INSERT INTO topic (title, description, topic_creator) VALUES ('General stuff', 'Everything between here and there and also some cats', 1);
INSERT INTO topic (title, description, topic_creator) VALUES ('Very top secret for alumnis', 'hush hush..', 2);
INSERT INTO topic (title, description, topic_creator) VALUES ('Such coding, much lines, wow.', 'Shiba doge telling the truths', 3);
INSERT INTO topic (title, description, topic_creator) VALUES ('Java Developer 2.0', 'The art of Java and how to Java your way through life', 1);

INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_group)
VALUES ('Share experiences', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo', 1, '2022-06-22 19:10:25-07', '2022-06-22 19:10:25-07', null, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_group)
VALUES ('My experiences', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 2, '2022-06-22 19:15:25-07', '2022-06-22 19:15:25-07', 1, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_group)
VALUES ('Working with a client', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo2', 3, '2022-06-22 19:20:25-07', '2022-06-22 19:20:25-07', 1, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_group)
VALUES ('Sounds great', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo', 1, '2022-08-22 19:10:25-07', '2022-08-22 19:10:25-07', 2, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, event_id)
VALUES ('A React problem', 'Can someone help me with following: .....', 4, '2022-06-23 19:10:25-07', '2022-06-23 19:10:25-07', null, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, event_id)
VALUES ('Solution', 'Check out this: www.reactTuorial.com', 3, '2022-06-23 19:15:25-07', '2022-06-23 19:15:25-07', 5, 1);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_topic)
VALUES ('React, Angular, Vue, which do you prefer?', 'Share your preferences.', 2, '2022-06-22 19:10:25-07', '2022-06-22 19:10:25-07', null, 3);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_topic)
VALUES ('Angular', 'Angular is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations.', 1,'2022-06-22 19:16:25-07', '2022-06-22 19:16:25-07', 7, 3);
INSERT INTO post (title, body, author, created_time, updated_time, original_post, target_topic)
VALUES ('Vue', 'Vue.js features an incrementally adaptable architecture that focuses on declarative rendering and component composition. The core library is focused on the view layer only. Advanced features....', 2, '2022-06-22 19:17:25-07', '2022-06-22 19:17:25-07', 7, 3);

INSERT INTO event_group_invite(event_id, group_id) VALUES (1,1);
INSERT INTO event_group_invite(event_id, group_id) VALUES (1,2);
INSERT INTO event_group_invite(event_id, group_id) VALUES (2,1);

INSERT INTO event_topic_invite(event_id, topic_id) VALUES (2,2);
INSERT INTO event_topic_invite(event_id, topic_id) VALUES (2,4);
INSERT INTO event_topic_invite(event_id, topic_id) VALUES (3,3);
INSERT INTO event_topic_invite(event_id, topic_id) VALUES (4,1);

INSERT INTO event_user_invite (event_id, user_id) VALUES (2,2);
INSERT INTO event_user_invite (event_id, user_id) VALUES (2,3);

INSERT INTO group_user(group_id, user_id) VALUES (1,1);
INSERT INTO group_user(group_id, user_id) VALUES (2,2);
INSERT INTO group_user(group_id, user_id) VALUES (1,2);

INSERT INTO group_user_join_request(group_id, user_id) VALUES (3,2);

INSERT INTO topic_user (topic_id, user_id) VALUES (4,2);
INSERT INTO topic_user (topic_id, user_id) VALUES (3,1);
INSERT INTO topic_user (topic_id, user_id) VALUES (1,3);
INSERT INTO topic_user (topic_id, user_id) VALUES (4,1);

INSERT INTO rsvp (event_id, user_id) VALUES (1,1);
INSERT INTO rsvp (event_id, user_id) VALUES (1,2);
INSERT INTO rsvp (event_id, user_id) VALUES (2,3);
INSERT INTO rsvp (event_id, user_id) VALUES (3,3);